## Avris Theme for Bootstrap 4 ##

A minimalist Bootstrap 4 theme

![](avris-theme.png)

### Installation

    npm install avris-theme

### Usage

    @import "variables"; // your variables
    @import "~avris-theme/style/variables";
    @import "~bootstrap/scss/bootstrap";
    @import "~avris-theme/avris-theme";

### Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
